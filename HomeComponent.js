
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Alert,
} from 'react-native';


var apiKey = "qDhff0APLvzJ0edwNVwqxNB5rvdKkXAOeJ5xCpeT";
export default class HomeComponent extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            astroidId: "",

        }
    }

    onSubmitClick() {
        console.log("onSubmitClick");
        this.getAsroidDetailById(this.state.astroidId);
    }
    onRandomClick() {
        console.log("onRandomClick")
        this.getRandomAstroidId();
    }

    getAsroidDetailById(astroidId){
        fetch('https://api.nasa.gov/neo/rest/v1/neo/' +astroidId + '?api_key=' + apiKey)
        .then(res=>res.json())
        .then(response=>{
            this.props.navigation.navigate('detail', { detail: response })

        }).catch((error)=>{
            Alert.alert("","Incorrect Astroid Id Entered");
        })
    }

    getRandomAstroidId(){
        fetch('https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=' + apiKey)
        .then(res=>res.json())
        .then(response=>{
            var random_index = Math.floor(Math.random() * Math.floor(response.page.size - 1));
            console.log(response.near_earth_objects[random_index].id);
            this.setState({
                astroidId:response.near_earth_objects[random_index].id
            },()=>{
                this.getAsroidDetailById(this.state.astroidId);
            })
        }).catch(error=>{
            Alert.alert("","Something went wrong");
        })
    }
    onTextChange = (text) => {
        if (text.trim() != "") {
            this.setState({ astroidId: text.trim() });
        }
    }
    render() {
        return (
            <View style={{padding:20}}>
                <TextInput placeholder="Enter Astroid Id"
                    onChangeText={(text) => this.onTextChange(text)} />

                <TouchableOpacity style={styles.btnStyle}
                    disabled={this.state.astroidId === "" ? true : false}
                    onPress={() => {
                        this.onSubmitClick()
                    }}
                >
                    <Text style={styles.btnTextStyle}>Submit</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btnStyle}
                    onPress={() => {
                        this.onRandomClick()
                    }}>
                    <Text style={styles.btnTextStyle}>Generate Random</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    btnStyle: {
        backgroundColor: "green",
        marginTop: 20,
        padding: 10,
    },
    btnTextStyle: {
        textAlign: "center",
        color: 'white'
    }
})
// const styles = StyleSheet.create({
//     obj_style: {
//         margin: 10,
//         backgroundColor: 'blue',
//         padding: 10,
//         justifyContent: "center"

//     }
// });