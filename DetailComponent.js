
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Alert,
} from 'react-native';


export default class DetailComponent extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data:this.props.route.params.detail
          }
    }
    
    componentDidMount(){
        // console.log("detail : "+this.state.data)

    }
    render(){
        return(
            <View>
            <Text style={styles.text_style}>Name : {this.state.data.name}</Text>
            <Text style={styles.text_style}>Nasa_Jpl_Url : {this.state.data.nasa_jpl_url}</Text>
            <Text style={styles.text_style}>Is_potentially_hazardous_asteroid : {this.state.data.is_potentially_hazardous_asteroid?"YES":"NO"}</Text>
  
              </View>
        )
    }

}

const styles = StyleSheet.create({
    text_style:{
        fontSize:20,
        padding:10
        
    },
})